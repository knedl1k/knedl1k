## Hey, I'm [knedl1k](https://github.com/knedl1k/)

<img align="right" height="280" width="450" alt="" src="https://raw.githubusercontent.com/snipe/animated-gifs/master/Techy/unecessary-automation.gif" />

### ⚙️ Things I use to get stuff done

- <b>OS:</b> &nbsp;Arch Linux
- <b>Browser: </b> &nbsp; LibreWolf and ungoogled-chromium
- <b>Terminal: </b> &nbsp;ZSH 
- <b>Code Editor:</b> &nbsp;neovim, nano, subl

</details>

#

<div align="center">

  ### Privacy is a human right.

</div>
